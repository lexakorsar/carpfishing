class PostImage < ActiveRecord::Base
  # belongs_to :post

  has_attached_file :attacment,
                    :styles => { :medium => "300x300>", :thumb => "100x100>" },
                    :url => "/attachments/user_:user/attacment/:basename_:style.:extension",
                    :path => ":rails_root/public/attachments/user_:user/attacment/:basename_:style.:extension"
  # :default_url => "/images/:style/missing.png"


end
