class Bait < ActiveRecord::Base
  belongs_to :bait_category

  attr_accessible :name, :description, :bait_category, :size, :bait_category_id
end
