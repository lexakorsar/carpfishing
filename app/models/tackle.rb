class Tackle < ActiveRecord::Base
  belongs_to :tackle_category

  attr_accessible :name, :description, :tackle_category, :tackle_category_id
end
