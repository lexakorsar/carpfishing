class Location < ActiveRecord::Base
  attr_accessible :name, :description, :lat, :lng
end
