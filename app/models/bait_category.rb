class BaitCategory < ActiveRecord::Base
  has_many :baits
  attr_accessible :name, :description
end
