class TackleCategory < ActiveRecord::Base
  has_many :tackles
  attr_accessible :name, :description
end
