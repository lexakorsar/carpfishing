class TripSession < ParseResource::Base
  fields :is_active, :location, :user, :startSession, :endSession

end