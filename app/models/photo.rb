class Photo < ActiveRecord::Base
  has_attached_file :image,
                    :styles => {:to_main=>"650x350#", :medium => "300x300>", :thumb => "100x100>" }

  attr_accessible :title, :image, :description

  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
end
