class Api::V1::LocationController < Api::V1::BaseController
  skip_before_filter :verify_authenticity_token,
                     :if => Proc.new { |c| c.request.format == 'application/json' }

  def index
    @locations = Location.all

    render :status => 200,
           :json => { :success => true,
                      :data => @locations.to_json }
  end
end
