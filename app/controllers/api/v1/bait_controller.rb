class Api::V1::BaitController < Api::V1::BaseController
  skip_before_filter :verify_authenticity_token,
                     :if => Proc.new { |c| c.request.format == 'application/json' }

  def index
    @baits= Bait.all

    render :status => 200,
           :json => { :success => true,
                      :data => @baits.to_json }
  end
end
