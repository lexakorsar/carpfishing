class Api::V1::TackleController < Api::V1::BaseController
  skip_before_filter :verify_authenticity_token,
                     :if => Proc.new { |c| c.request.format == 'application/json' }

  def index
    @tackles = Tackle.all

    render :status => 200,
           :json => { :success => true,
                      :data => @tackles.to_json }
  end
end
