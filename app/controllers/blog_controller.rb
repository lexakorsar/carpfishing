class BlogController < ApplicationController
  before_filter :tag_cloud, :blog_cloud

  def tag_cloud
    @tags = Post.tag_counts_on(:tags)
  end

  def index
    @posts = Post.order('created_at DESC').paginate(:page => params[:page], :per_page => 8)

  end

  def show
    @post = Post.find(params[:id])
  end

  def tag_show
    @posts = Post.tagged_with(params[:id]).order('created_at DESC')
    # render :action => :index
  end


  def blog_cloud
    @articles_by_month = Post.all.order('created_at DESC').group_by { |article|  Russian::strftime(article.created_at,"%B %Y") }
  end
end
