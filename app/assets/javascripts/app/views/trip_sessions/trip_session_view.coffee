DiaryTrip.Views.TripSessions ||= {}

class DiaryTrip.Views.TripSessions.TripSessionView extends Backbone.View
  template: JST["app/templates/trip_sessions/trip_session"]

  events:
    "click .destroy" : "destroy"

  tagName: "tr"

  destroy: () ->
    @model.destroy()
    this.remove()

    return false

  render: ->
    $(@el).html(@template(@model.toJSON() ))
    return this