DiaryTrip.Views.TripSessions ||= {}

class DiaryTrip.Views.TripSessions.IndexView extends DiaryTrip.Views.BaseView
  template: JST["app/templates/trip_sessions/index_view"]
  table: "locations"

  initialize: () ->
    DiaryTrip.Views.BaseView.prototype.initialize.apply(this, arguments);

  addOne: (trip_session) =>
    view = new DiaryTrip.Views.TripSessions.TripSessionView({model : trip_session})
    @$("tbody").append(view.render().el)

  beforeRender: =>
    @$el.html(@template(trip_session: @collection.toJSON() ))