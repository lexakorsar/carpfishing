class DiaryTrip.Views.TripSessions.ShowAllChartView extends Backbone.View
  template: JST["app/templates/trip_sessions/show_all_chart_view"]

  tagName: "div"
  className: "col-sm-12"

  render: ->
    $(@el).html(@template(@model.toJSON()))
    session_chart = @init_chart @el, @model
#    console.log(@session_chart)
    @requestData @model, session_chart
    return this


  requestData: (model, session_chart) ->
    query = new Parse.Query(DiaryTrip.Models.TripSessionItem)
    query.equalTo "trip_session", model

    collection = query.collection()

    time = new Array()
    data = new Array()
    collection.fetch
      success: (collection) ->
        collection.each (object) ->
          weather_query = new Parse.Query(DiaryTrip.Models.SessionWetherItem)
          weather_query.equalTo "rod_item", object
          weather_collection = weather_query.collection()
          weather_collection.fetch
            success: (weather_collection) ->
              weather_collection.each (object) ->
                data.push(parseFloat(object.get("temperature")))
                time.push(object.createdAt.yyyymmdd())
              series = {
                name: object.get("nozzle"),
                data: data
              }
              session_chart.addSeries(series);
              session_chart.xAxis[0].setCategories(time)
              return
            error: (collection, error) ->
        return

      error: (collection, error) ->
    return

  init_chart: (el) ->
#    console.log(roods)

    $container = $(el).find("#session_chart")
    session_chart = new Highcharts.Chart(
      chart:
        renderTo: $container[0]
#        defaultSeriesType: 'spline'
##        events:
##          load: @requestData(model)

      title:
        text: "Статистика клева в течении сессии"
        x: -20

      subtitle:
        text: "При поддержке магазина - 'Рыбацкие страсти'"
        x: -20

      xAxis:
        categories: []

      yAxis:
        title:
          text: "Температура (°C)"

        plotLines: [
          value: 0
          width: 1
          color: "#808080"
        ]

      tooltip:
        valueSuffix: "°C"

      legend:
        layout: "vertical"
        align: "right"
        verticalAlign: "middle"
        borderWidth: 0


    )

    return session_chart

