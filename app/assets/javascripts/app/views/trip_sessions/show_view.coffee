DiaryTrip.Views.TripSessions ||= {}

class DiaryTrip.Views.TripSessions.ShowView extends Backbone.View
  template: JST["app/templates/trip_sessions/show_view"]

  render: ->
    $(@el).html(@template(@model.toJSON()))

    @renderRoods(@model)

    @renderStats(@model)

    return this



  renderStats: (session) ->
    Parse.Cloud.run "sessionsAddFish", { trip_session: session.id },
      success: (result) ->
        $("#session-info span.add-fish").text(result)
      error: (error) ->

    Parse.Cloud.run "sessionsReleaseFish", { trip_session: session.id },
      success: (result) ->
        $("#session-info span.release-fish").text(result)
      error: (error) ->

    Parse.Cloud.run "sessionsNullFish", { trip_session: session.id },
      success: (result) ->
        $("#session-info span.null-fish").text(result)
      error: (error) ->

  renderRoods: (session) ->
    query = new Parse.Query(DiaryTrip.Models.TripSessionItem)
    query.equalTo "trip_session", session

    collection = new DiaryTrip.Collections.TripSessionItemsCollection();

    collection= query.collection()

    collection.fetch
      success: (collection) ->
        collection.each (object) ->
          @view = new DiaryTrip.Views.TripSessions.ShowItemView(model: object)
          $("#roods-panel").append(@view.render().el)
#          console.warn object
          return
        return

      error: (collection, error) ->

