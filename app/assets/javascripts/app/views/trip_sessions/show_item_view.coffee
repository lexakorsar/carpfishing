DiaryTrip.Views.TripSessions ||= {}

class DiaryTrip.Views.TripSessions.ShowItemView extends Backbone.View
  template: JST["app/templates/trip_sessions/show_item_view"]

  tagName: "div"
  className: "col-xs-6 col-md-3",

  render: ->
    $(@el).html(@template(@model.toJSON()))
    @renderStats(@model)
    return this

  renderStats: (rood) ->
#    Parse.Cloud.run "sessionsRodsAddFish", { trip_session_item: rood.id, trip_session: rood.get('trip_session').id },
#      success: (result) ->
#        $("#rod-info-" + rood.id + " span.add-fish").text(result)
#      error: (error) ->
#
#    Parse.Cloud.run "sessionsRodsReleaseFish", { trip_session_item: rood.id, trip_session: rood.get('trip_session').id },
#      success: (result) ->
#        $("#rod-info-" + rood.id + " span.release-fish").text(result)
#      error: (error) ->
#
#    Parse.Cloud.run "sessionsRodsNullFish", { trip_session_item: rood.id, trip_session: rood.get('trip_session').id },
#      success: (result) ->
#        $("#rod-info-" + rood.id + " span.null-fish").text(result)
#      error: (error) ->