DiaryTrip.Views.Locations ||= {}

class DiaryTrip.Views.Locations.IndexView extends DiaryTrip.Views.BaseView
  template: JST["app/templates/locations/index_view"]
  table: "locations"

  initialize: () ->
    DiaryTrip.Views.BaseView.prototype.initialize.apply(this, arguments);

  addOne: (location) =>
    view = new DiaryTrip.Views.Locations.LocationView({model : location})
    @$("tbody").append(view.render().el)

  beforeRender: =>
    @$el.html(@template(location: @collection.toJSON() ))