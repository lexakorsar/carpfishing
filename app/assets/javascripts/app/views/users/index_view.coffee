DiaryTrip.Views.Users ||= {}

class DiaryTrip.Views.Users.IndexView extends DiaryTrip.Views.BaseView
  template: JST["app/templates/users/index"]
  table: "users"

  initialize: () ->
    DiaryTrip.Views.BaseView.prototype.initialize.apply(this, arguments);

  addOne: (user) =>
    view = new DiaryTrip.Views.Users.UserView({model : user})
    @$("tbody").append(view.render().el)

  beforeRender: =>
    @$el.html(@template(location: @collection.toJSON() ))