DiaryTrip.Views.Users ||= {}

class DiaryTrip.Views.Users.UserView extends Backbone.View
  template: JST["app/templates/users/user"]

  events:
    "click .destroy" : "destroy"

  tagName: "tr"

  destroy: () ->
    @model.destroy()
    this.remove()

    return false

  render: ->
    $(@el).html(@template(@model.toJSON() ))
    return this