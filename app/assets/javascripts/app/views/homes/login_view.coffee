DiaryTrip.Views.Homes ||= {}

class DiaryTrip.Views.Homes.LoginView extends Backbone.View

  template: JST["app/templates/homes/sign_in"]

  events: ->
    'click button#sign_in': "sign_in"

  sign_in: (event) ->
    Parse.User.logIn($("#inputLogin").val(), $("#inputPassword").val(),{
      success: (user) ->
#        @view = new Telekopilka.Views.Homes.IndexView()
#        $("#main").html(@view.render().el)
        window.location.reload()
      error: (user, error) ->
        console.log(error.code)
#        @notice = new DiaryTrip.Models.Notice
#        @notice.set({error: "Неправильные данные для авторизации", type: "danger"})
#        @view = new DiaryTrip.Views.Components.NoticeView(model: @notice)
#        @view.render()
#        alert("nologin")
    })
  render: ->
    $(@el).html(@template())
    return this