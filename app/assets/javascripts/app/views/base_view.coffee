DiaryTrip.Views ||= {}

class DiaryTrip.Views.BaseView extends Backbone.View
  template: ""
  table: ""


  initialize: () ->
    @render = _.wrap @render, (render) =>
      @beforeRender()
      render()
#      setTimeout(@afterRender(), 800)
      @afterRender()
      @



  addAll: ->
    @collection.each @.addOne

  renderErr: (model, attrs) ->

  beforeRender: =>


  afterRender: =>
#    @renderTable

  renderTable: =>
    if $("table#"+@table+"-table")
      $("table#"+@table+"-table").dataTable
        sDom: "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>"
        sPaginationType: "bootstrap"
        aoColumnDefs: [
          bSortable: false
          aTargets: [0, -1]
        ]
        oLanguage:
          sLengthMenu: "Отобразить _MENU_ записей на странице"
          sZeroRecords: "Записей нет"
          sInfo: "Найдено _TOTAL_ "
          sInfoEmpty: " "
          sInfoFiltered: "(из _MAX_ записей)"
          sSearch: "Поиск"
          oPaginate:
            sFirst:    "в начало"
            sPrevious: "назад"
            sNext:     "вперед"
            sLast:     "в конец"
    return this

  render: =>
    @$el.html(@template(collection: @collection.toJSON() ))
    @addAll()
    return this