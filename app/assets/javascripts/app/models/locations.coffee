class DiaryTrip.Models.Location extends Parse.Object
  className: "Locations"

  defaults:
    name: null
    description: null
    coordinates: null

class DiaryTrip.Collections.LocationsCollection extends Parse.Collection
  model: DiaryTrip.Models.Location
#  url: '/users'