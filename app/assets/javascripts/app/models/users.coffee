class DiaryTrip.Models.User extends Parse.User

  defaults:
    username: null


class DiaryTrip.Collections.UsersCollection extends Parse.Collection
  model: DiaryTrip.Models.User