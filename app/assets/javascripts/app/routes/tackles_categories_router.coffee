class DiaryTrip.Routers.TacklesCategoriesRouter extends Backbone.SubRoute
  initialize: (options) ->


  routes:
    "new"      : "newTacklesCategories"
    "index": "indexTacklesCategories"
    ":id/edit": "editTacklesCategories"
    ":id": "showTacklesCategories"
    ".*": "indexTacklesCategories"