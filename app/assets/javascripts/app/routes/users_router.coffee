class DiaryTrip.Routers.UsersRouter extends Backbone.SubRoute
  initialize: (options) ->
    @users = new DiaryTrip.Collections.UsersCollection


  routes:
    "new"      : "newUsers"
    "index": "indexUsers"
    ":id/edit": "editUsers"
    ":id": "showUsers"
    ".*": "indexUsers"


  indexUsers: ->
    @users.fetch
      success: (collection) ->
        @view = new DiaryTrip.Views.Users.IndexView(collection: collection)
        $("#main").html(@view.render().el)
      error: ->
        console.error "Can't fetch"  if console