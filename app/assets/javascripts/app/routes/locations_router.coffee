class DiaryTrip.Routers.LocationsRouter extends Backbone.SubRoute
  initialize: (options) ->
    @locations = new DiaryTrip.Collections.LocationsCollection


  routes:
    "new"      : "newLocation"
    "index": "indexLocation"
    ":id/edit": "editLocation"
    ":id": "showLocation"
    ".*": "indexLocation"


  indexLocation: ->
    @locations.fetch
      success: (collection) ->
        @view = new DiaryTrip.Views.Locations.IndexView(collection: collection)
        $("#main").html(@view.render().el)
      error: ->
        console.error "Can't fetch"  if console