class DiaryTrip.Routers.BaitsRouter extends Backbone.SubRoute
  initialize: (options) ->


  routes:
    "new"      : "newBait"
    "index": "indexBait"
    ":id/edit": "editBait"
    ":id": "showBait"
    ".*": "indexBait"