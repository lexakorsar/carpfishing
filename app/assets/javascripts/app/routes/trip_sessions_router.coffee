class DiaryTrip.Routers.TripSessionsRouter extends Backbone.SubRoute
  initialize: (options) ->
    @trip_sessions = new DiaryTrip.Collections.TripSessionsCollection


  routes:
    "new"      : "newTripSessions"
    "index": "indexTripSessions"
    ":id/edit": "editTripSessions"
    ":id/show-all-chart": "showAllChart"
    ":id": "showTripSessions"
    ".*": "indexTripSessions"



  indexTripSessions: ->
    @trip_sessions.fetch
      success: (collection) ->
        @view = new DiaryTrip.Views.TripSessions.IndexView(collection: collection)
        $("#main").html(@view.render().el)



      error: ->
        console.error "Can't fetch"  if console


  showTripSessions: (id) ->
    query = new Parse.Query(DiaryTrip.Models.TripSessions);
    query.get id,
      success: (results) ->
        @view = new DiaryTrip.Views.TripSessions.ShowView(model: results)
        $("#main").html(@view.render().el)
      error: (model, error) ->
        if (error.code is Parse.Error.OBJECT_NOT_FOUND)
          alert("Uh oh, we couldn't find the object!")

  showAllChart: (id) ->
    query = new Parse.Query(DiaryTrip.Models.TripSessions);
    query.get id,
      success: (results) ->
        @view = new DiaryTrip.Views.TripSessions.ShowAllChartView(model: results)
        $("#main").html(@view.render().el)
      error: (model, error) ->
        if (error.code is Parse.Error.OBJECT_NOT_FOUND)
          alert("Uh oh, we couldn't find the object!")