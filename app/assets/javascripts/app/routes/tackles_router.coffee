class DiaryTrip.Routers.TacklesRouter extends Backbone.SubRoute
  initialize: (options) ->


  routes:
    "new"      : "newTackles"
    "index": "indexTackles"
    ":id/edit": "editTackles"
    ":id": "showTackles"
    ".*": "indexTackles"