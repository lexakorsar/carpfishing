class DiaryTrip.Routers.BaitsCategoriesRouter extends Backbone.SubRoute
  initialize: (options) ->


  routes:
    "new"      : "newBaitCategory"
    "index": "indexBaitCategory"
    ":id/edit": "editBaitCategory"
    ":id": "showBaitCategory"
    ".*": "indexBaitCategory"