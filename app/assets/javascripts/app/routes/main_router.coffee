class DiaryTrip.Routers.MainRouter extends Backbone.Router

  routes:
    "": "index",

    "users": "users",
    "users/*subroute"  : "users",

    "locations": "locations",
    "locations/*subroute"  : "locations",

    "baits": "baits",
    "baits/*subroute"  : "baits",

    "baits-categories": "baitsCategories",
    "baits-categories/*subroute"  : "baitsCategories",

    "tackles": "tackles",
    "tackles/*subroute"  : "tackles",

    "tackles-categories": "tacklesCategories",
    "tackles-categories/*subroute"  : "tacklesCategories",

    "trip-sessions": "tripSessions",
    "trip-sessions/*subroute"  : "tripSessions",




  index: ->
    if Parse.User.current()
      if !window.usersRouter
        window.usersRouter = new DiaryTrip.Routers.UsersRouter("users")
        window.usersRouter.navigate("/index", {trigger: true});
    else
      @view = new DiaryTrip.Views.Homes.LoginView()
      return $("#main").html(@view.render().el)

  users:(subroute) ->
    if Parse.User.current()
      if !window.usersRouter
        window.usersRouter = new DiaryTrip.Routers.UsersRouter("users")
        window.usersRouter.navigate("/index", {trigger: true});
    else
      @view = new DiaryTrip.Views.Homes.LoginView()
      return $("#main").html(@view.render().el)

  locations:(subroute) ->
    if Parse.User.current()
      if !window.locationsRouter
        window.locationsRouter = new DiaryTrip.Routers.LocationsRouter("locations")
        window.locationsRouter.navigate("/index", {trigger: true});
    else
      @view = new DiaryTrip.Views.Homes.LoginView()
      return $("#main").html(@view.render().el)

  baits:(subroute) ->
    if Parse.User.current()
      if !window.baitsRouter
        window.baitsRouter = new DiaryTrip.Routers.BaitsRouter("baits")
        window.baitsRouter.navigate("/index", {trigger: true});
    else
      @view = new DiaryTrip.Views.Homes.LoginView()
      return $("#main").html(@view.render().el)

  baitsCategories:(subroute) ->
    if Parse.User.current()
      if !window.baitsCategories
        window.baitsCategories = new DiaryTrip.Routers.BaitsCategoriesRouter("baits_categories")
        window.baitsCategories.navigate("/index", {trigger: true});
    else
      @view = new DiaryTrip.Views.Homes.LoginView()
      return $("#main").html(@view.render().el)

  tackles:(subroute) ->
    if Parse.User.current()
      if !window.tacklesRouter
        window.tacklesRouter = new DiaryTrip.Routers.TacklesRouter("tackles")
        window.tacklesRouter.navigate("/index", {trigger: true});
    else
      @view = new DiaryTrip.Views.Homes.LoginView()
      return $("#main").html(@view.render().el)

  tacklesCategories:(subroute) ->
    if Parse.User.current()
      if !window.tacklesCategoriesRouter
        window.tacklesCategoriesRouter = new DiaryTrip.Routers.TacklesCategoriesRouter("tackles_categories")
        window.tacklesCategoriesRouter.navigate("/index", {trigger: true});
    else
      @view = new DiaryTrip.Views.Homes.LoginView()
      return $("#main").html(@view.render().el)

  tripSessions:(subroute) ->
    if Parse.User.current()
      if !window.tripSessions
        window.tripSessions = new DiaryTrip.Routers.TripSessionsRouter("trip_sessions")
        window.tripSessions.navigate("index", {trigger: true});
    else
      @view = new DiaryTrip.Views.Homes.LoginView()
      return $("#main").html(@view.render().el)

