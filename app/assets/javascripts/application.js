// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
// require jquery_ujs
// require jquery.ui.all
//= require turbolinks
//= require libs/jquery-dataTables
//= require libs/underscore
//= require libs/backbone
//= require libs/subroute
//= require libs/parse
//= require bootstrap
//= require libs/highcharts/highcharts
//= require_tree .


$(function() {
    $('.nav li a').click(function(e) {
//        e.preventDefault();
        $(this).parent().addClass('active').siblings().removeClass('active');
    });

//    $("#partnersTab a").click(function(e) {
//        return $(this).parent().addClass('active').siblings().removeClass('active');
//    });

    Date.prototype.yyyymmdd = function() {
        var yyyy = this.getFullYear().toString();
        var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
        var dd  = this.getDate().toString();
        return (dd[1]?dd:"0"+dd[0]) +"-" + (mm[1]?mm:"0"+mm[0]) +"-"+ yyyy ; // padding
    };

});

//
//$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
//{
//    return {
//        "iStart":         oSettings._iDisplayStart,
//        "iEnd":           oSettings.fnDisplayEnd(),
//        "iLength":        oSettings._iDisplayLength,
//        "iTotal":         oSettings.fnRecordsTotal(),
//        "iFilteredTotal": oSettings.fnRecordsDisplay(),
//        "iPage":          oSettings._iDisplayLength === -1 ?
//            0 : Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
//        "iTotalPages":    oSettings._iDisplayLength === -1 ?
//            0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
//    };
//};
//
///* Bootstrap style pagination control */
//$.extend( $.fn.dataTableExt.oPagination, {
//    "bootstrap": {
//        "fnInit": function( oSettings, nPaging, fnDraw ) {
//            var oLang = oSettings.oLanguage.oPaginate;
//            var fnClickHandler = function ( e ) {
//                e.preventDefault();
//                if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
//                    fnDraw( oSettings );
//                }
//            };
//
//            $(nPaging).addClass('').append(
//                '<ul class="pagination">'+
//                    '<li class="prev disabled"><a href="#">&larr; '+oLang.sPrevious+'</a></li>'+
//                    '<li class="next disabled"><a href="#">'+oLang.sNext+' &rarr; </a></li>'+
//                    '</ul>'
//            );
//            var els = $('a', nPaging);
//            $(els[0]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
//            $(els[1]).bind( 'click.DT', { action: "next" }, fnClickHandler );
//        },
//
//        "fnUpdate": function ( oSettings, fnDraw ) {
//            var iListLength = 5;
//            var oPaging = oSettings.oInstance.fnPagingInfo();
//            var an = oSettings.aanFeatures.p;
//            var i, ien, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);
//
//            if ( oPaging.iTotalPages < iListLength) {
//                iStart = 1;
//                iEnd = oPaging.iTotalPages;
//            }
//            else if ( oPaging.iPage <= iHalf ) {
//                iStart = 1;
//                iEnd = iListLength;
//            } else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
//                iStart = oPaging.iTotalPages - iListLength + 1;
//                iEnd = oPaging.iTotalPages;
//            } else {
//                iStart = oPaging.iPage - iHalf + 1;
//                iEnd = iStart + iListLength - 1;
//            }
//
//            for ( i=0, ien=an.length ; i<ien ; i++ ) {
//                // Remove the middle elements
//                $('li:gt(0)', an[i]).filter(':not(:last)').remove();
//
//                // Add the new list items and their event handlers
//                for ( j=iStart ; j<=iEnd ; j++ ) {
//                    sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
//                    $('<li '+sClass+'><a href="#">'+j+'</a></li>')
//                        .insertBefore( $('li:last', an[i])[0] )
//                        .bind('click', function (e) {
//                            e.preventDefault();
//                            oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
//                            fnDraw( oSettings );
//                        } );
//                }
//
//                // Add / remove disabled classes from the static elements
//                if ( oPaging.iPage === 0 ) {
//                    $('li:first', an[i]).addClass('disabled');
//                } else {
//                    $('li:first', an[i]).removeClass('disabled');
//                }
//
//                if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
//                    $('li:last', an[i]).addClass('disabled');
//                } else {
//                    $('li:last', an[i]).removeClass('disabled');
//                }
//            }
//        }
//    }
//} );

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};


window.router = new DiaryTrip.Routers.MainRouter();
Backbone.history.start();
