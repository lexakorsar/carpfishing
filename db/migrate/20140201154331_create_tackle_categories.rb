class CreateTackleCategories < ActiveRecord::Migration
  def change
    create_table :tackle_categories do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
