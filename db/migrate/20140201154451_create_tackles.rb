class CreateTackles < ActiveRecord::Migration
  def change
    create_table :tackles do |t|
      t.string :name
      t.text :description
      t.references :tackle_category, index: true

      t.timestamps
    end
  end
end
