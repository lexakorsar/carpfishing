class BlogAdminModify < ActiveRecord::Migration
  def change
    change_table :blog_admins do |t|

      t.attachment :avatar
      t.string :name
      t.string :title
      t.text :small_description
      t.text :description
      t.text :interests
    end
  end
end
