class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.text :small_description
      t.text :description
      t.references :post_images

      t.timestamps
    end
  end
end
