class AddAttachmentAttachmentToPostImages < ActiveRecord::Migration
  def self.up
    change_table :post_images do |t|
      t.attachment :attachment
    end
  end

  def self.down
    drop_attached_file :post_images, :attachment
  end
end
