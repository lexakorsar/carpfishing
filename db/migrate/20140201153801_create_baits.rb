class CreateBaits < ActiveRecord::Migration
  def change
    create_table :baits do |t|
      t.string :name
      t.text :description
      t.integer :size
      t.references :bait_category, index: true

      t.timestamps
    end
  end
end
