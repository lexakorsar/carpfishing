# encoding: UTF-8

RailsAdmin.config do |config|

  if defined?(WillPaginate)
    module WillPaginate
      module ActiveRecord
        module RelationMethods
          def per(value = nil) per_page(value) end
          def total_count() count end
        end
      end
      module CollectionMethods
        alias_method :num_pages, :total_pages
      end
    end
  end

  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :blog_admin
  end
  config.current_user_method(&:current_blog_admin)

  config.authorize_with do
    redirect_to new_blog_admin_session_url unless warden.user.admin?
  end

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  config.included_models = %w(ActsAsTaggableOn::Tag BlogAdmin Post Photo)

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    history_index
    history_show
  end

  config.model Post do
    edit do
      field :title
      field :small_description
      field :title_image
      field :blog_admin
      field :tag_list
      field :description, :ck_editor
    end

    list do
      field :blog_admin
      field :title
      field :small_description
      field :title_image
      field :description
      field :tag_list
      field :created_at
    end
  end

  config.model Photo do

  end

  # config.models do
  #   edit do
  #     fields_of_type :tag_list do
  #       partial 'tag_list_with_suggestions'
  #     end
  #   end
  # end



end
