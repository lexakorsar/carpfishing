set :stage, :production

set :deploy_to, '~/apps/carpfishing_club'

set :branch, 'master'

set :rails_env, 'production'

# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary
# server in each group is considered to be the first
# unless any hosts have the primary property set.
role :app, %w{carpfishing@89.253.221.197}
role :web, %w{carpfishing@89.253.221.197}
role :db,  %w{carpfishing@89.253.221.197}