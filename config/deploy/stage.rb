set :stage, :stage
set :branch, :master
set :rails_env, "production"
set :deploy_to, '/home/fishingtrip/projects/diary_fishing_trip'
set :unicorn_pid,     "/home/fishingtrip/projects/diary_fishing_trip/shared/tmp/pids/fishing_trip.pid"
set :unicorn_start_cmd, "(cd #{deploy_to}/current; rvm use ruby-2.0.0-p195@fishingtrip do #{fetch(:bundle_binstubs)}/unicorn_rails -Dc /etc/unicorn/fishing_trip.rb)"



# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary
# server in each group is considered to be the first
# unless any hosts have the primary property set.
role :web, %w{88.198.0.42}
role :app, %w{88.198.0.42}
role :db, %w{88.198.0.42}

# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server
# definition into the server list. The second argument
# something that quacks like a hash can be used to set
# extended properties on the server.
server '88.198.0.42', user: 'fishingtrip', roles: %w{web app}

# you can set custom ssh options
# it's possible to pass any option but you need to keep in mind that net/ssh understand limited list of options
# you can see them in [net/ssh documentation](http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start)
# set it globally
#  set :ssh_options, {
#    keys: %w(/home/rlisowski/.ssh/id_rsa),
#    forward_agent: false,
#    auth_methods: %w(password)
#  }
# and/or per server
server '88.198.0.42',
       user: 'fishingtrip',
       roles: %w{web app},
       ssh_options: {
           user: 'fishingtrip', # overrides user setting above
           #keys: %w(/home/telekopilka/.ssh/id_rsa),
           forward_agent: true,
           auth_methods: %w(password),
           password: 'hs,fkrfnhbg'
       }
# setting per server overrides global ssh_options

# fetch(:default_env).merge!(rails_env: :staging)



namespace :unicorn do
  unicorn_pid = "/home/fishingtrip/projects/diary_fishing_trip/shared/tmp/pids/fishing_trip.pid"

  def run_unicorn
    execute "#{fetch(:bundle_binstubs)}/unicorn_rails", "-Dc /etc/unicorn/fishing_trip.rb -E #{fetch(:stage)}"
  end

  desc 'Start unicorn'
  task :start do
    on roles(:app) do
      run_unicorn
    end
  end

  desc 'Stop unicorn'
  task :stop do
    on roles(:app) do
      if test "[ -f #{unicorn_pid} ]"
        execute :kill, "-QUIT `cat #{unicorn_pid}`"
      end
    end
  end

  desc 'Force stop unicorn (kill -9)'
  task :force_stop do
    on roles(:app) do
      if test "[ -f #{unicorn_pid} ]"
        execute :kill, "-9 `cat #{unicorn_pid}`"
        execute :rm, unicorn_pid
      end
    end
  end

  desc 'Restart unicorn'
  task :restart do
    on roles(:app) do
      if test "[ -f #{unicorn_pid} ]"
        execute :kill, "-QUIT `cat #{unicorn_pid}`"
      end
      run_unicorn
      #run "[ -f #{unicorn_pid} ] && kill -USR2 `cat #{unicorn_pid}` || #{run_unicorn}"
    end
  end
end