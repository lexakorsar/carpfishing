DiaryFishingTripService::Application.routes.draw do

  root "main#index"

  devise_for :blog_admin, controllers: { sessions: 'blog_admin/sessions' }

  mount RailsAdmin::Engine => '/blog_admin', as: 'rails_admin'

  get '/locations', to: 'locations#index', as: 'locations'
  get '/blog', to: 'blog#index'

  get '/blog/:id', to: 'blog#show', as: 'post'
  # match 'blog/by_year_and_month/:year/:month' => 'blog#by_year_and_month', :as=> :posts_by_year_and_month

  get '/blog//tag/:id', to: 'blog#tag_show', as: 'tag'

  # get '/team', to: 'team#index'
  # get '/video', to: 'team#video', as: 'video'
  get '/team/:id', to: 'team#show', as: 'profile'



  #devise_for :users

  namespace :admin do
    root "main#index"

  end

  namespace :profile do
    root "main#index"

  end

  mount Ckeditor::Engine => '/ckeditor'

  namespace :api do
    namespace :v1 do
      devise_scope :user do
        post 'registrations' => 'registrations#create', :as => 'register'
        post 'sessions' => 'sessions#create', :as => 'login'
        delete 'sessions' => 'sessions#destroy', :as => 'logout'
      end
      get 'locations' => 'location#index', :as => 'locations'
      get 'tackles' => 'tackle#index', :as => 'tackles'
      get 'baits' => 'bait#index', :as => 'baits'
    end
  end


end
